Exercise 4 - Wireshark in kali

Wireshark in kali
1. Boot up Kali
2. start wireshark, and listen on interface eth0
3. in a terminal, ping 8.8.8.8
4. refind the packets in wireshark
5. select the echo ping request packet
6. what are the MAC adresses and ip adresses?
	MAC = VMware_f5:b3:19 (00:0c:29:f5:b3:19)
	IP = 192.168.1.15

Notice the layered info, starting with "frame", "ethernet" and "Internet protocol"

7. Explain what each of the 4 values means and which devices they refer to
	The frame is used by Wireshark as a base for all the protocols. It shows information from capturing. 
	Ethernet can be use to capture packets that are being transmitted and received between the machine that are running Wireshark or any other machine that are running on the network.
	The Internet Protocol provides the network layer transport functionality in the InterProtocolFamily.

8. Ping 1.1.1.1
9. Refind the packets in wireshark
10. select the echo ping request packet
11. What are the MAC adresses and ip adresses?
	MAC = VMware:f5:b3:19 (00:0c:29:f5:b3:19)
	IP Address = 192.168.1.15
	The IP address and MAC address the only thing that is different is the destination.
12. Explain what each of the 4 values means and which devices they refer to
	There is no differences.
13. Compare values for the two pings and explain similarities/differences
14. Put addresses and descriptions in a shared document